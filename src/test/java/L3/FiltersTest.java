package L3;

import ib.zpo2.L3.AllCustomerImages;
import ib.zpo2.L3.Filters;
import ib.zpo2.L3.Image;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDate;
import java.util.ArrayList;

import static ib.zpo2.L3.AllCustomerImages.encodeAndStore;
import static ib.zpo2.L3.AllCustomerImages.findAllFilesInFolder;
import static org.junit.jupiter.api.Assertions.assertEquals;

class FiltersTest {

    @Test
    void filterWithParameters() throws NoSuchAlgorithmException, InvalidKeySpecException {

        ArrayList<Image> test= new ArrayList<Image>();

        AllCustomerImages images = new AllCustomerImages();
        String wholePath = "src/main/java/ib/zpo2/L2/imageDirectory/1975";
        File folder = new File(wholePath);
        findAllFilesInFolder(folder, images);
        encodeAndStore(images.getImageFiels(),images.getMetadataFiels(), "1975", "nolitimerequiaegosumtecum", images);

        // For filtering without setting the parameters
        Filters filter = new Filters();
        filter.filter(images.getImages());
        assertEquals(1, filter.filter(images.getImages()).size());

        // For filtering with startData set – images in selected period
        filter.setStartDate(LocalDate.of(1990, 01,01));
        filter.setEndDate(LocalDate.of(2022, 06,20));
        assertEquals("Stworzenie inne niż wszystkie :-)", filter.filter(images.getImages()).get(0).getMetadata().getDescription());

        // For filtering with startData set – no images in selected period
        filter.setStartDate(LocalDate.of(2000, 11,23));
        filter.setEndDate(LocalDate.of(2022, 06,20));
        assertEquals(test, filter.filter(images.getImages()));

        // For filtering with title set – images of selected title
        filter.setEndDate(null);
        filter.setStartDate(null);
        filter.setTitle("Matura '94");
        assertEquals("Matura '94", filter.filter(images.getImages()).get(0).getMetadata().getTitle());

        // For filtering with title set – no images of selected title
        filter.setTitle("Matura 2019");
        assertEquals(test, filter.filter(images.getImages()));

        filter.setTitle(null);

        filter.setDescription(":-)");
        assertEquals("Stworzenie inne niż wszystkie :-)", filter.filter(images.getImages()).get(0).getMetadata().getDescription());

        filter.setDescription(":-(");
        assertEquals(test, filter.filter(images.getImages()));

    }
}