package L1;

import ib.zpo2.L1.Token;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TokenTester {
    @Test
    @DisplayName("Check if tokenDecode() returns correct value.")
    public void isSeviceConfigCorrect() {
        Token token = Token.tokenDecode("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdXN0b21lclRva2VuIjoiUEw0MzI1ZXhfZmJhMTEwZDAtNDg0OC00ZGMwLTllOGQtYjExNDFjZmRkNjRlMTIuNDMuNTQuNi4yLjEiLCJuYW1lIjoiSm9obiBEb2UiLCJpYXQiOjE1MTYyMzkwMjJ9.MeoGAq-tptTG6_eW7IF9140Hh6ho6BXAH3pBnuEMWtY");
        assertEquals(token.getHeader(), "{\"alg\":\"HS256\",\"typ\":\"JWT\"}");
        assertEquals(token.getPayload(), "{\"customerToken\":\"PL4325ex_fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1\",\"name\":\"John Doe\",\"iat\":1516239022}");
    }
}
