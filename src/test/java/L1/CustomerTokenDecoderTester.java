package L1;

import ib.zpo2.L1.Token;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static ib.zpo2.L1.Token.tokenDecode;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class CustomerTokenDecoderTester {

    @Test
    @DisplayName("Check if the tokenDecode() returns correct token1 — in case it does.")
// service id = [12]
// payload
//{
//  "customerToken": "GB666ex_ccc110d0-4848-4dc0-9e8d-b1141cfdd64e12",
//  "name": "John Doe",
//  "iat": 1516239022
//}
    public void isTokenDecodeReturnsCorrectToken() {
        Token token1 = new Token("{\"alg\":\"HS256\",\"typ\":\"JWT\"}", "{\"customerToken\":\"PL4325ex_fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1\",\"name\":\"John Doe\",\"iat\":1516239022}");
        Token token2 = tokenDecode("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdXN0b21lclRva2VuIjoiUEw0MzI1ZXhfZmJhMTEwZDAtNDg0OC00ZGMwLTllOGQtYjExNDFjZmRkNjRlMTIuNDMuNTQuNi4yLjEiLCJuYW1lIjoiSm9obiBEb2UiLCJpYXQiOjE1MTYyMzkwMjJ9.MeoGAq-tptTG6_eW7IF9140Hh6ho6BXAH3pBnuEMWtY");

        assertEquals(token1.getHeader(), token2.getHeader());
        assertEquals(token1.getPayload(), token2.getPayload());

    }


    @Test
    @DisplayName("Check if the tokenDecode() returns correct token1 — in case it doesn't.")
// service id = [12]
// payload
//{
//  "customerToken": "GB666ex_ccc110d0-4848-4dc0-9e8d-b1141cfdd64e12",
//  "name": "John Doe",
//  "iat": 1516239022
//}
    public void isTokenDecodeReturnsIncorrectToken() {
        Token token1 = new Token("{\"alg\":\"HS255\",\"typ\":\"JWW\"}", "{\"customerToken\":\"BG666ex_ccc110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1\",\"name\":\"John Hoe\",\"iat\":1516239022}");
        Token token2 = tokenDecode("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdXN0b21lclRva2VuIjoiUEw0MzI1ZXhfZmJhMTEwZDAtNDg0OC00ZGMwLTllOGQtYjExNDFjZmRkNjRlMTIuNDMuNTQuNi4yLjEiLCJuYW1lIjoiSm9obiBEb2UiLCJpYXQiOjE1MTYyMzkwMjJ9.MeoGAq-tptTG6_eW7IF9140Hh6ho6BXAH3pBnuEMWtY");

        assertNotEquals(token1.getHeader(), token2.getHeader());
        assertNotEquals(token1.getPayload(), token2.getPayload());

    }

}
