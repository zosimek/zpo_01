package L1;

import ib.zpo2.L1.Customer;
import ib.zpo2.L1.customer.CountryCode;
import ib.zpo2.L1.customer.Id;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

public class CustomerTester {
    @Test
    @DisplayName("Check that the Customer constructor constructs a valid Customer object — in case it does.")
    public void countryCodeCorrectFormat() {
        CountryCode countryCode = new CountryCode("PL");
        Id id = new Id("4325");
        UUID externalId = UUID.fromString("fba110d0-4848-4dc0-9e8d-b1141cfdd64e");
        List enabledServices = new ArrayList<Integer>();
        enabledServices.addAll(Arrays.asList(("12.43.54.6.2.1").split("\\.")));
        Customer customer = new Customer(countryCode, id, externalId, enabledServices);
        assertEquals(customer.getCountryCode().getCountryCode().toString(), "PL");
        assertEquals(customer.getId().getId().toString(), "4325");
        assertEquals(customer.getExternalId().toString(), "fba110d0-4848-4dc0-9e8d-b1141cfdd64e");
        assertEquals(customer.getEnableServices().toString(), "[12, 43, 54, 6, 2, 1]");
        System.out.println(customer.getId().getId());
    }
}
