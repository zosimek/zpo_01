package L1;

import ib.zpo2.L1.CustomerTokenValidator;
import ib.zpo2.L1.customer.CountryCode;
import ib.zpo2.L1.customer.Id;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class CustomerTokenValidatorTester {

    List<String> id_1 = Arrays.asList("12");
    List<String> id_2 = Arrays.asList("12", "2");
    List<String> id_3 = Arrays.asList("12", "43", "54", "6", "2", "1");

    CountryCode countryCode = new CountryCode("PL");
    Id id = new Id("4325");
    UUID uuid = UUID.fromString("fba110d0-4848-4dc0-9e8d-b1141cfdd64e");

    CustomerTokenValidator customerTokenValidator = new CustomerTokenValidator();

    @Test
    @DisplayName("Check if token contains the customerToken — in case it has not")
    // service id = [12]
    // payload
    //{
    //  "sub": "1234567890",
    //  "name": "John Doe",
    //  "iat": 1516239022
    //}
    public void jwtHasCustomerTokenFalse() {
        assertThrows(IllegalStateException.class, new Executable() {

            @Override
            public void execute() throws Throwable {
                customerTokenValidator.isValid(id_1, "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9" +
                        ".eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MD" +
                        "IyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c");
            }
        });
    }

    @Test
    @DisplayName("Check if customerToken has the right format — in case it has not")
    // service id = [12]
    // payload
    //{
    //  "customerToken": "incorrect",
    //  "sub": "1234567890",
    //  "name": "John Doe",
    //  "iat": 1516239022
    //}
    public void customerTokenRightFormatFalse() {
        assertThrows(IllegalStateException.class, new Executable() {

            @Override
            public void execute() throws Throwable {
                customerTokenValidator.isValid(id_1, "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9." +
                        "eyJjdXN0b21lclRva2VuIjoiaW5jb3JyZWN0Iiwic3ViIjoiMTIzNDU2Nzg5MCIsIm5hbW" +
                        "UiOiJKb2huIERvZSIsImlhdCI6MTUxNjIzOTAyMn0.2UszlfcYAqEJGBnTKTlHv65JPczkY" +
                        "JYCy8df1lwVM1o");
            }
        });
    }


    @Test
    @DisplayName("Check if the customer has access to service (id = [12]) — in case it has not")
    // service id = [12]
    // payload
    //{
    //  "customerToken": "PL4325ex_fba110d0-4848-4dc0-9e8d-b1141cfdd64e43.54.6.2.1",
    //  "name": "John Doe",
    //  "iat": 1516239022
    //}
    public void customerHasAccessFalse_01() {
        assertEquals(customerTokenValidator.isValid(id_1, "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.ey" +
                "JjdXN0b21lclRva2VuIjoiUEw0MzI1ZXhfZmJhMTEwZDAtNDg0OC00ZGMwLTllOGQtYjExNDF" +
                "jZmRkNjRlNDMuNTQuNi4yLjEiLCJuYW1lIjoiSm9obiBEb2UiLCJpYXQiOjE1MTYyMzkwMjJ9" +
                ".LkOBq3qNojGRtOzEOUk0pY6oMb4AzVW4dMOLh_typlg"), false);

    }


    @Test
    @DisplayName("Check if the customer has access to service (id = [12, 2]) — in case it has to one and not to the other.")
    // service id = [12]
    // payload
    //{
    //  "customerToken": "PL4325ex_fba110d0-4848-4dc0-9e8d-b1141cfdd64e43.54.6.2.1",
    //  "name": "John Doe",
    //  "iat": 1516239022
    //}
    public void customerHasAccessFalse_02() {
        assertEquals(customerTokenValidator.isValid(id_2, "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.ey" +
                "JjdXN0b21lclRva2VuIjoiUEw0MzI1ZXhfZmJhMTEwZDAtNDg0OC00ZGMwLTllOGQtYjExNDF" +
                "jZmRkNjRlNDMuNTQuNi4yLjEiLCJuYW1lIjoiSm9obiBEb2UiLCJpYXQiOjE1MTYyMzkwMjJ9" +
                ".LkOBq3qNojGRtOzEOUk0pY6oMb4AzVW4dMOLh_typlg"), false);
    }

    @Test
    @DisplayName("Check if the isValid() method returns correct Customer class object — in case it does.")
    // service id = [12]
    // payload
    //{
    //  "customerToken": "PL4325ex_fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1",
    //  "name": "John Doe",
    //  "iat": 1516239022
    //}
    public void isValidReturnsCorrectCustomer() {
        Boolean valid = customerTokenValidator.isValid(id_1, "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdXN0b21lclRva2VuIjoiUEw0MzI1ZXhfZmJhMTEwZDAtNDg0OC00ZGMwLTllOGQtYjExNDFjZmRkNjRlMTIuNDMuNTQuNi4yLjEiLCJuYW1lIjoiSm9obiBEb2UiLCJpYXQiOjE1MTYyMzkwMjJ9.MeoGAq-tptTG6_eW7IF9140Hh6ho6BXAH3pBnuEMWtY");
        assertEquals(valid, true);
    }


    @Test
    @DisplayName("Check if the isValid() method returns correct Customer class object — in case it doesn't.")
    // service id = [12]
    // payload
    //{
    //  "customerToken": "GB666ex_ccc110d0-4848-4dc0-9e8d-b1141cfdd64e12",
    //  "name": "John Doe",
    //  "iat": 1516239022
    //}
    public void isValidReturnsIncorrectCustomer() {
        Boolean valid1 = customerTokenValidator.isValid(id_1, "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdXN0b21lclRva2VuIjoiR0I2NjZleF9jY2MxMTBkMC00ODQ4LTRkYzAtOWU4ZC1iMTE0MWNmZGQ2NGUxMiIsIm5hbWUiOiJKb2huIERvZSIsImlhdCI6MTUxNjIzOTAyMn0.3Ae-jLf6DBL2ATf4nu5ARC90-uTsZTJfGWQhnOvk5Bc");
        assertNotEquals(valid1, false);
    }

}
