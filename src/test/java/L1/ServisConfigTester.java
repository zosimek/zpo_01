package L1;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static ib.zpo2.L1.ServiceConfig.getId;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ServisConfigTester {
    @Test
    @DisplayName("Check if ServiceConfig class mathod getId() returns correct value.")
    public void isSeviceConfigCorrect() {
        List<String> expected = Arrays.asList("12");
        assertEquals(getId(), expected);
    }
}
