package L1;

import ib.zpo2.L1.Customer;
import ib.zpo2.L1.customer.CountryCode;
import ib.zpo2.L1.customer.Id;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class IdTester {

    @Test
    @DisplayName("Check that the Id constructor constructs a valid Id object — in case it does.")
    public void idCorrectFormat() {
        Id id1 = new Id("1234");

        assertEquals(id1.getId(), "1234");

    }

    @Test
    @DisplayName("Check that the Id constructor constructs a valid Id object — in case it does not.")
    public void idWrongFormat() {
        assertThrows(IllegalStateException.class, new Executable() {
        @Override
        public void execute() throws Throwable {
            Id id2 = new Id("1a3c5d");
            }
        });
    }

}
