package L1;

import ib.zpo2.L1.customer.CountryCode;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import javax.validation.ValidationException;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CountryCodeTester {

    @Test
    @DisplayName("Check if countryCode is the wright format — in case it has not")
    public void countryCodeWrongFormat() {
        assertThrows(ValidationException.class, new Executable() {

            @Override
            public void execute() throws Throwable {
                CountryCode countryCode = new CountryCode("PLN");
            }
        });
    }


    @Test
    @DisplayName("Check if countryCode has the wright format — in case it has")
    public void countryCodeCorrectFormat() {
        CountryCode countryCode = new CountryCode("PL");
        assertEquals(countryCode.getCountryCode().toString(), "PL");

    }

}
