package L2;

import ib.zpo2.L2.AESKey;
import ib.zpo2.L2.CustomerMainKey;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import static org.junit.Assert.assertEquals;

public class CustomerMainKeyTest {

    @Test
    @DisplayName("Check if CustomerMainKey decrypts mainKey correctly — in case it does.")
    public void CustomerMainKeyCorrect() throws NoSuchAlgorithmException, InvalidKeySpecException {
        String mainKey = "e40f57da-115a-4875-9055-d774f6253a43";
        String decryptedMainKey = CustomerMainKey.decryptMainKey("1975", "nolitimerequiaegosumtecum");

        assertEquals(mainKey, decryptedMainKey);
    }
}
