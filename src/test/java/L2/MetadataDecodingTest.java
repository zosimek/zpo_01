package L2;

import ib.zpo2.L2.ImageDecoding;
import ib.zpo2.L2.MetaDecoding;
import ib.zpo2.L2.MetaEncoding;
import org.json.JSONObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import static org.junit.Assert.assertEquals;

public class MetadataDecodingTest {

    JSONObject metadataJson = MetaEncoding.metaFileToJson("src/main/java/ib/zpo2/L2/files/metadata1.txt");
    String metadataString = MetaEncoding.jsonToXml(metadataJson);

    public MetadataDecodingTest() throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
    }

//    Dunno why the test does nit pass, although compared values are identical.
    @Test
    @DisplayName("Check if MetadataDecoding.imageDecoder decodes correctly — in case it does.")
    public void imageDecoderCorrect() throws NoSuchAlgorithmException, InvalidKeySpecException {
        JSONObject metadataDecrypted =  MetaDecoding.metadataDecoder("1975", "nolitimerequiaegosumtecum", "src/main/java/ib/zpo2/L2/imageDirectory/1975/metadata.Matura '94.txt");

        assertEquals(metadataDecrypted, metadataJson);
    }
}
