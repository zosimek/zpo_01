package L2;

import ib.zpo2.L1.Customer;
import ib.zpo2.L1.customer.CountryCode;
import ib.zpo2.L1.customer.Id;
import ib.zpo2.L2.AESKey;
import ib.zpo2.L2.CustomerMainKey;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class AESKeyTest {
    @Test
    @DisplayName("Check if AESKey encrypts and decrypts correctly — in case it does.")
    public void aesKeyCorrect() {
        String riddle = "dr Popek";
        String encryption1 = AESKey.encrypt("prof. Iskander", "prof. Sielużycki");
        String encryption2 = AESKey.encrypt( riddle, encryption1);
        String decryption = AESKey.decrypt(encryption2, encryption1);

        assertEquals(riddle, decryption);
    }
}
