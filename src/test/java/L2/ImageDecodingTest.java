package L2;

import ib.zpo2.L2.AESKey;
import ib.zpo2.L2.ImageDecoding;
import ib.zpo2.L2.ImageEncoding;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import static org.junit.Assert.assertEquals;

public class ImageDecodingTest {
    String imgPath1 = "src/main/java/ib/zpo2/L2/files/LO_8_matura_94.jpg";
    byte[] imageBytes = ImageEncoding.imgToByteArray(imgPath1);
    String imgBase64 = ImageEncoding.imageBase64Encoder(imageBytes);


    public ImageDecodingTest() throws IOException {
    }


    @Test
    @DisplayName("Check if ImageDecoding.imageDecoder decodes correctly — in case it does.")
    public void imageDecoderCorrect() throws NoSuchAlgorithmException, InvalidKeySpecException {
        String imageString = ImageDecoding.imageDecoder("1975", "nolitimerequiaegosumtecum", "src/main/java/ib/zpo2/L2/imageDirectory/1975/Matura '94.txt");

        assertEquals(imgBase64, imageString);
    }


}
