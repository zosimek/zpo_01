package ib.zpo2.L2;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.FileNotFoundException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Scanner;

public class CustomerMainKey {
    public static String decryptMainKey(String customerId, String password) throws NoSuchAlgorithmException, InvalidKeySpecException{
        String customersXMLFilePath = "src/main/java/ib/zpo2/L2/customerXML/customerXML.xlm";
        String encryptedMianKey = null;
        String salt = null;

//        https://www.w3schools.com/java/java_files_read.asp#:~:text=try%20%7B%0A%20%20%20%20%20%20File%20myObj%20%3D,printStackTrace()%3B%0A%20%20%20%20%7D
        String data = null;
        try {
            File myObj = new File(customersXMLFilePath);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                data = myReader.nextLine();
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        assert data != null;
        JSONObject obj = XML.toJSONObject(data);
        JSONObject customers = (JSONObject) obj.get("collection");

        try {
            JSONObject customer = (JSONObject) customers.get(customerId);
            encryptedMianKey = (String) customer.get("key");
            salt = (String) customer.get("salt");
        } catch (JSONException e) {
            System.out.println("User does not exist!");
            e.printStackTrace();
        }


        String secretKey = password + salt;
//        System.out.println("\n MainKey \n");
//        System.out.println(password);
//        System.out.println(salt);
//        System.out.println(encryptedMianKey);
        String mainKey = AESKey.decrypt(encryptedMianKey, secretKey);

        return mainKey;
    }
}
