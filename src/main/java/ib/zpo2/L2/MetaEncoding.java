package ib.zpo2.L2;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Scanner;
import java.util.jar.JarEntry;

public class MetaEncoding {

//    https://stackoverflow.com/questions/33638765/how-to-read-json-data-from-txt-file-in-java#:~:text=BufferedReader%20reader%20%3D,%7B%0A%20%20%20%20reader.close()%3B%0A%7D
//    The function returns image's metadata in JSON
    public static JSONObject metaFileToJson(String metaPath) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(metaPath));
        String json = "";
        try {
            StringBuilder sb = new StringBuilder();
            String line = reader.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = reader.readLine();
            }
            json = sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            reader.close();
        }
        JSONObject object = new JSONObject(json); // this will get you the entire JSON node
        return object;
    }

    public static String jsonToXml(JSONObject json){
        String xml_data = XML.toString(json);
        return xml_data;
    }

    public static void metadataEncoder(String metadata, String customerId, String customerPassword, String metadataPath) throws NoSuchAlgorithmException, InvalidKeySpecException {

        String title = null;

//        https://www.w3schools.com/java/java_files_read.asp#:~:text=try%20%7B%0A%20%20%20%20%20%20File%20myObj%20%3D,printStackTrace()%3B%0A%20%20%20%20%7D
        String data = null;
        try {
            File myObj = new File(metadataPath);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                data = myReader.nextLine();
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        assert data != null;
        JSONObject obj = new JSONObject(data);

        try {
            title = (String) obj.get("title");
        } catch (JSONException e) {
            System.out.println("User does not exist!");
            e.printStackTrace();
        }

        Path path = Paths.get("src/main/java/ib/zpo2/L2/imageDirectory/" + customerId + "/metadata." + title + ".txt");
        String decryptedMainKey = CustomerMainKey.decryptMainKey(customerId, customerPassword);
        String encryptedMetadata = AESKey.encrypt(metadata, decryptedMainKey);

        try {

            Files.writeString(path, encryptedMetadata, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            System.out.print("Invalid Path");
        }

    }

}
