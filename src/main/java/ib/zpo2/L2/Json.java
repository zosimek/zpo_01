package ib.zpo2.L2;

import ib.zpo2.L1.Customer;
import ib.zpo2.L1.customer.Id;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Random;

public class Json {

    // https://www.tutorialspoint.com/how-to-write-create-a-json-file-using-java#:~:text=import%20java.,%C2%A0%20%C2%A0%7D%0A%7D
    // https://stackoverflow.com/questions/42916873/how-to-create-multi-level-json-data-using-jsonobject-in-servlet/42916959#:~:text=That%20is%20very,println(json.toString())%3B

    public static JSONObject customerToJson(JSONObject jsonCustomers, Customer customer) {

        String secretKey = customer.getPassword() + customer.getSalt().toString();
        String encryptedMainKey = AESKey.encrypt(customer.getKey(), secretKey);

//        System.out.println(customer.getKey());
//        System.out.println(customer.getSalt());
//        System.out.println(customer.getPassword());
//        System.out.println(secretKey);
//        System.out.println(encryptedMainKey);

        JSONObject jsonCustomerData = new JSONObject();
        jsonCustomerData.put("key", encryptedMainKey);
        jsonCustomerData.put("salt", customer.getSalt().toString());

        jsonCustomers.put(customer.getId().getId(), jsonCustomerData);
        return jsonCustomers;
    }
}
