package ib.zpo2.L2;

import org.json.JSONObject;
import org.json.XML;

import java.io.File;
import java.io.FileNotFoundException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Scanner;

public class MetaDecoding {

    public static JSONObject metadataDecoder(String customerId, String customerPassword, String metadataPath) throws NoSuchAlgorithmException, InvalidKeySpecException {

        String metadataEncrypted = null;

        try {
            File metadataEncoded = new File(metadataPath);
            Scanner myReader = new Scanner(metadataEncoded);
            while (myReader.hasNextLine()) {
                metadataEncrypted = myReader.nextLine();
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        String decryptedMainKey = CustomerMainKey.decryptMainKey(customerId, customerPassword);
        String decryptedMetadata = AESKey.decrypt(metadataEncrypted, decryptedMainKey);

        JSONObject metadataJSON = XML.toJSONObject(decryptedMetadata);
        return metadataJSON;
    }
}
