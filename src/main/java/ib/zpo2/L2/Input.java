package ib.zpo2.L2;

import javax.json.Json;
import java.awt.image.BufferedImage;

public class Input {
    private BufferedImage image;
    private Json metadata;
    private String password;

    public Input(BufferedImage image, Json metadata, String password) {
        this.image = image;
        this.metadata = metadata;
        this.password = password;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public Json getMetadata() {
        return metadata;
    }

    public void setMetadata(Json metadata) {
        this.metadata = metadata;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
