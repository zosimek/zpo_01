package ib.zpo2.L2;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.Random;
import java.util.Scanner;

public class ImageEncoding {

//    The function returns byte array of an image
    public static byte[] imgToByteArray(String imgPath) throws IOException{
        // read image from file
        FileInputStream stream = new FileInputStream(imgPath);
        // get byte array from image stream
        int bufLength = 2048;
        byte[] buffer = new byte[2048];
        byte[] data;

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int readLength;
        while ((readLength = stream.read(buffer, 0, bufLength)) != -1) {
            out.write(buffer, 0, readLength);
        }

        data = out.toByteArray();
        out.close();
        stream.close();
        return data;
    }


//    The function returns image encoded in base64
    public static String imageBase64Encoder(byte[] data) throws IOException {

        String imageString = Base64.getEncoder().withoutPadding().encodeToString(data);
        Base64.getEncoder().withoutPadding().encodeToString(data);
        return imageString;
    }

    public static void imageEncoder(String imageBase64, String customerId, String customerPassword, String metadataPath) throws NoSuchAlgorithmException, InvalidKeySpecException {

        String title = null;

//        https://www.w3schools.com/java/java_files_read.asp#:~:text=try%20%7B%0A%20%20%20%20%20%20File%20myObj%20%3D,printStackTrace()%3B%0A%20%20%20%20%7D
        String data = null;
        try {
            File myObj = new File(metadataPath);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                data = myReader.nextLine();
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        assert data != null;
        JSONObject obj = new JSONObject(data);

        try {
            title = (String) obj.get("title");
        } catch (JSONException e) {
            System.out.println("User does not exist!");
            e.printStackTrace();
        }

        Path path = Paths.get("src/main/java/ib/zpo2/L2/imageDirectory/" + customerId + "/" + title + ".txt");
        String decryptedMainKey = CustomerMainKey.decryptMainKey(customerId, customerPassword);
        String encryptedImage = AESKey.encrypt(imageBase64, decryptedMainKey);

        try {

            Files.writeString(path, encryptedImage, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            System.out.print("Invalid Path");
        }

    }
}
