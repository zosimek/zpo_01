package ib.zpo2.L2;

import org.json.JSONObject;
import org.json.XML;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
import java.util.Scanner;

public class ImageDecoding {

    public static String imageDecoder(String customerId, String customerPassword, String imagePath) throws NoSuchAlgorithmException, InvalidKeySpecException {

        String imageEncrypted = null;

        try {
            File metadataEncoded = new File(imagePath);
            Scanner myReader = new Scanner(metadataEncoded);
            while (myReader.hasNextLine()) {
                imageEncrypted = myReader.nextLine();
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        String decryptedMainKey = CustomerMainKey.decryptMainKey(customerId, customerPassword);
        String decryptedImage = AESKey.decrypt(imageEncrypted, decryptedMainKey);
        return decryptedImage;
    }

    public static byte[] imageBase64Decoder(String stringImage){
        byte[] decodedImage = Base64.getDecoder().decode(stringImage);
        return decodedImage;
    }

//    NOT NEEDED
//    public static void imageSave(byte[] byteImage) throws IOException {
//        ByteArrayInputStream inStreambj = new ByteArrayInputStream(byteImage);
//        BufferedImage newImage = ImageIO.read(inStreambj);
//        ImageIO.write(newImage, "jpg", new File("outputImage.jpg"));
//        System.out.println("Image generated from the byte array.");
//    }
}
