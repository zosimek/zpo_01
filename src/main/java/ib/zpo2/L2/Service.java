package ib.zpo2.L2;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.UUID;

public class Service {

    String imgDir = getImgDir("src/main/java/ib/zpo2/L2/configFile.txt");

    public static String getImgDir(String fileName) {
        String data = null;
        try {
            File configFile = new File(fileName);
            Scanner myReader = new Scanner(configFile);
            while (myReader.hasNextLine()) {
                data = myReader.nextLine();
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return data;
    }
}
