package ib.zpo2.L2;

import com.thoughtworks.xstream.XStream;
import ib.zpo2.L1.Customer;
import ib.zpo2.L1.customer.CountryCode;
import ib.zpo2.L1.customer.Id;
import org.json.JSONObject;
import org.json.XML;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class Try2 {
    public static void main(String[] args) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {

//        encrypt images with Base64
        String imgPath1 = "src/main/java/ib/zpo2/L2/files/LO_8_matura_94.jpg";
        String imgBase64_1 = ImageEncoding.imageBase64Encoder(ImageEncoding.imgToByteArray(imgPath1));
//        System.out.println(imgBase64_1);
//        System.out.println("\n");

        String imgPath2 = "src/main/java/ib/zpo2/L2/files/Profesor-Robert-Iskander.jpg";
        String imgBase64_2 = ImageEncoding.imageBase64Encoder(ImageEncoding.imgToByteArray(imgPath2));
//        System.out.println(imgBase64_2);
//        System.out.println("\n");

        String imgPath3 = "src/main/java/ib/zpo2/L2/files/miejsce_pracy_lokalnego_zlotnika.jpg";
        String imgBase64_3 = ImageEncoding.imageBase64Encoder(ImageEncoding.imgToByteArray(imgPath3));
//        System.out.println(imgBase64_3);
//        System.out.println("\n");

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // create couple of Customers
        //commented out to prevent from changing Customers attributes

        CountryCode countryCode = new CountryCode("PL");
        Id id1 = new Id("1975");
        UUID externalId1 = UUID.fromString("fba110d0-4848-4dc0-9e8d-b1141cfdd64e");
        List enabledServices1 = new ArrayList<Integer>();
        enabledServices1.addAll(Arrays.asList(("12.43.54.6.2.1").split("\\.")));
        Customer customer1 = new Customer(countryCode, id1, "nolitimerequiaegosumtecum", externalId1, enabledServices1);
//
//        Id id2 = new Id("1989");
//        UUID externalId2 = UUID.fromString("fba110d0-4478-4dc0-9d5d-b1141cfdd64e");
//        List enabledServices2 = new ArrayList<Integer>();
//        enabledServices2.addAll(Arrays.asList(("12.64.54.3.72.9").split("\\.")));
//        Customer customer2 = new Customer(countryCode, id2, "wLoDzImIeRzWySocKi_RaJsKiEjAbŁkA", externalId2, enabledServices2);
//
//        Id id3 = new Id("1963");
//        UUID externalId3 = UUID.fromString("fba110d0-4298-4dc0-9d5d-b1141cfdd64e");
//        List enabledServices3 = new ArrayList<Integer>();
//        enabledServices3.addAll(Arrays.asList(("12.23.13.7.2.3").split("\\.")));
//        Customer customer3 = new Customer(countryCode, id3, "c1i2ek3a4w5e_p1o2c3h4o5d6z7e8n9i10e11_e1t2n3i4c5z6n7e8", externalId3, enabledServices3);
//
//
//        System.out.println(customer1.getKey());
//        System.out.println(customer2.getKey());
//        System.out.println(customer3.getKey());
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//        create List to store Customers
//        commented out to prevent from changing Customers attributes

//        List customerList = new ArrayList();
//        customerList.add(customer1);
//        customerList.add(customer2);
//        customerList.add(customer3);
//        System.out.println(customerList);
//        System.out.println("\n");

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//        put all Customers nicely in json
//        commented out to prevent from changing Customers attributes

//        JSONObject json1 = new JSONObject();
//        org.json.simple.JSONObject json2 = new org.json.simple.JSONObject();
//
//        ib.zpo2.L2.Json.customerToJson(json2, customer1);
//        ib.zpo2.L2.Json.customerToJson(json2, customer2);
//        ib.zpo2.L2.Json.customerToJson(json2, customer3);
//        json1.put("collection", json2);
//        System.out.println(json1);
//        System.out.println("\n");

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//        put the json nicely in XML file
//        commented out to prevent from changing Customers attributes

//        String jsonString = json1.toString();
//        JSONObject json_try = new JSONObject(jsonString);
//        String xml_data = XML.toString(json_try);
//        System.out.println(xml_data);
//        System.out.println("\n");

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//        converting XML file to JSON
//        commented out to prevent from changing Customers attributes

//        JSONObject obj = XML.toJSONObject(xml_data);
//        System.out.println(obj.toString());
//        System.out.println("\n");

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//        create nice XML file from customerList
//        commented out to prevent from changing Customers attributes

//        XStream xstream = new XStream();
//        String xml = xstream.toXML(customerList);
//        System.out.println(xml);

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//        save XML created above to file
        Path path = Paths.get("src/main/java/ib/zpo2/L2/customerXML.xlm");

//        commented out to prevent from changing Customers attributes

//        try {
//            // Now calling Files.writeString() method
//            // with path , content & standard charsets
//            Files.writeString(path, xml_data,
//                    StandardCharsets.UTF_8);
//        } catch (IOException ex) {
//            // Print messqage exception occurred as
//            // invalid. directory local path is passed
//            System.out.print("Invalid Path");
//        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//        create json from metadata

        JSONObject jsonMeta1 = new JSONObject();
        jsonMeta1.put("author", "unknown");
        jsonMeta1.put("title", "Matura '94");
        jsonMeta1.put("location", "Wroclaw");
        jsonMeta1.put("date", "1994-04-05");
        jsonMeta1.put("description", "Stworzenie inne niż wszystkie :-) ");
        jsonMeta1.put("format", "jpg");
//        System.out.println(jsonMeta1);
        Path pathMeta1 = Paths.get("src/main/java/ib/zpo2/L2/files/metadata1.txt");

        JSONObject jsonMeta2 = new JSONObject();
        jsonMeta2.put("author", "Mateusz Popek");
        jsonMeta2.put("title", "Miejsce pracy lokalnego złotnika");
        jsonMeta2.put("location", "Wroclaw");
        jsonMeta2.put("date", "2022-06-19");
        jsonMeta2.put("description", "Niesamowicei oryginalna osobistość :-)");
        jsonMeta2.put("format", "jpg");
//        System.out.println(jsonMeta2);
        Path pathMeta2 = Paths.get("src/main/java/ib/zpo2/L2/files/metadata2.txt");

        JSONObject jsonMeta3 = new JSONObject();
        jsonMeta3.put("author", "Agencja Gazeta");
        jsonMeta3.put("title", "Jakiś na pewno");
        jsonMeta3.put("location", "Wroclaw");
        jsonMeta3.put("date", "2016-02-28");
        jsonMeta3.put("description", "Przepiękne i mądre stworzenie :-)");
        jsonMeta3.put("format", "jpg");
//        System.out.println(jsonMeta3);
        Path pathMeta3 = Paths.get("src/main/java/ib/zpo2/L2/files/metadata3.txt");

//        commented out to prevent overwriting files L2/files/metadata1-3.txt

//        try {
//            // Now calling Files.writeString() method
//            // with path , content & standard charsets
//            Files.writeString(pathMeta1, jsonMeta1.toString(),
//                    StandardCharsets.UTF_8);
//            Files.writeString(pathMeta2, jsonMeta2.toString(),
//                    StandardCharsets.UTF_8);
//            Files.writeString(pathMeta3, jsonMeta3.toString(),
//                    StandardCharsets.UTF_8);
//        } catch (IOException ex) {
//            // Print messqage exception occurred as
//            // invalid. directory local path is passed
//            System.out.print("Invalid Path");
//        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//        decrypt mainKeys of Customers to encrypt images and their metadata

        String readMetadata1 = MetaEncoding.jsonToXml(MetaEncoding.metaFileToJson("src/main/java/ib/zpo2/L2/files/metadata1.txt"));
        String readMetadata2 = MetaEncoding.jsonToXml(MetaEncoding.metaFileToJson("src/main/java/ib/zpo2/L2/files/metadata2.txt"));
        String readMetadata3 = MetaEncoding.jsonToXml(MetaEncoding.metaFileToJson("src/main/java/ib/zpo2/L2/files/metadata3.txt"));


        String decryptedMainKey1 = CustomerMainKey.decryptMainKey("1975", "nolitimerequiaegosumtecum");
        System.out.println(decryptedMainKey1);
        String decryptedMainKey2 = CustomerMainKey.decryptMainKey("1989", "wLoDzImIeRzWySocKi_RaJsKiEjAbŁkA");
        System.out.println(decryptedMainKey2);
        String decryptedMainKey3 = CustomerMainKey.decryptMainKey("1963", "c1i2ek3a4w5e_p1o2c3h4o5d6z7e8n9i10e11_e1t2n3i4c5z6n7e8");
        System.out.println(decryptedMainKey3);

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//        encrypting metedata and image
//        commented out to prevent from overwriting filed in imageDirectory directory.

//        String encryptedImage1 = AESKey.encrypt(imgBase64_1, decryptedMainKey1);
//        String encryptedMeta1 = AESKey.encrypt(readMetadata1, decryptedMainKey1);
//        ImageEncoding.imageEncoder(imgBase64_1, "1975", "nolitimerequiaegosumtecum", "src/main/java/ib/zpo2/L2/files/metadata1.txt");
//        MetaEncoding.metadataEncoder(readMetadata1, "1975", "nolitimerequiaegosumtecum", "src/main/java/ib/zpo2/L2/files/metadata1.txt");
//
//
//        String encryptedImage2 = AESKey.encrypt(imgBase64_2, decryptedMainKey2);
//        String encryptedMeta2 = AESKey.encrypt(readMetadata2, decryptedMainKey2);
//        ImageEncoding.imageEncoder(imgBase64_2, "1989", "wLoDzImIeRzWySocKi_RaJsKiEjAbŁkA", "src/main/java/ib/zpo2/L2/files/metadata2.txt");
//        MetaEncoding.metadataEncoder(readMetadata2, "1989", "wLoDzImIeRzWySocKi_RaJsKiEjAbŁkA", "src/main/java/ib/zpo2/L2/files/metadata2.txt");
//
//
//        String encryptedImage3 = AESKey.encrypt(imgBase64_3, decryptedMainKey3);
//        String encryptedMeta3 = AESKey.encrypt(readMetadata3, decryptedMainKey3);
//        ImageEncoding.imageEncoder(imgBase64_3, "1963", "c1i2ek3a4w5e_p1o2c3h4o5d6z7e8n9i10e11_e1t2n3i4c5z6n7e8", "src/main/java/ib/zpo2/L2/files/metadata3.txt");
//        MetaEncoding.metadataEncoder(readMetadata3, "1963", "c1i2ek3a4w5e_p1o2c3h4o5d6z7e8n9i10e11_e1t2n3i4c5z6n7e8", "src/main/java/ib/zpo2/L2/files/metadata3.txt");

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//        decrypting metadata
//        commented just because I do not need it to pop up on my screen right now

//        JSONObject metadataDecrypted1 =  MetaDecoding.metadataDecoder("1975", "nolitimerequiaegosumtecum", "src/main/java/ib/zpo2/L2/imageDirectory/1975/metadata.Matura '94.txt");
//        System.out.println(metadataDecrypted1);
//
//        JSONObject metadataDecrypted2 =  MetaDecoding.metadataDecoder("1989", "wLoDzImIeRzWySocKi_RaJsKiEjAbŁkA", "src/main/java/ib/zpo2/L2/imageDirectory/1989/metadata.Miejsce pracy lokalnego złotnika.txt");
//        System.out.println(metadataDecrypted2);
//
//        JSONObject metadataDecrypted3 =  MetaDecoding.metadataDecoder("1963", "c1i2ek3a4w5e_p1o2c3h4o5d6z7e8n9i10e11_e1t2n3i4c5z6n7e8", "src/main/java/ib/zpo2/L2/imageDirectory/1963/metadata.Jakiś na pewno.txt");
//        System.out.println(metadataDecrypted3);

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//        String imageString = ImageDecoding.imageDecoder("1975", "nolitimerequiaegosumtecum", "src/main/java/ib/zpo2/L2/imageDirectory/1975/Matura '94.txt");
//        byte[] imageByte = ImageDecoding.imageBase64Decoder(imageString);
//        ImageDecoding.imageSave(imageByte);
    }
}
