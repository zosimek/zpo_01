package ib.zpo2.L4;

import java.util.ArrayList;
import java.util.UUID;

// Class describing NOTE JSON thingy from List 4
public class JSONNote extends Folder {

    private final String id;

    public JSONNote(ArrayList<Folder> folders, ArrayList<Note> notes, String title){
        super(folders, notes, title);
        this.id = UUID.randomUUID().toString();
    }

    public JSONNote(Folder folder){
        super(folder.getFolders(), folder.getNotes(), folder.getTitle());
        this.id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    // That the heck is that
    @Override
    public String toString(){
        return "{\n\"name\":\"" + getTitle() + "\",\n\"note\":\n{" + super.toString() + "\n}}";
    }
}
