package ib.zpo2.L4;

import java.util.ArrayList;

public class Folder {

    private ArrayList<Folder> Folders;
    private ArrayList<Note> notes;
    private final String title;

    public Folder(ArrayList<Folder> Folders, ArrayList<Note> notes, String title) {
        this.Folders = Folders;
        this.notes = notes;
        this.title = title;
    }

    public ArrayList<Folder> getFolders() {
        return Folders;
    }

    public void setFolders(ArrayList<Folder> Folders) {
        this.Folders = Folders;
    }

    public ArrayList<Note> getNotes() {
        return notes;
    }

    public void setNotes(ArrayList<Note> notes) {
        this.notes = notes;
    }

    public String getTitle() {
        return title;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (notes != null)
            for (Note note : notes)
                builder.append("\"").append(note.getTitle()).append("\":\"").append(note.getContent()).append("\",");
        if (Folders != null)
            for (Folder subFolder : Folders)
                builder.append("\"").append(subFolder.getTitle()).append("\":{").append(subFolder).append("\"}");
            String builderToString = builder.toString();
            if(builderToString.length() > 0)
            return builderToString.substring(0, builderToString.length() - 1);
            return builderToString;

    }
}
