package ib.zpo2.L4;

import org.json.JSONObject;

import java.util.ArrayList;

public class NoteCreator {
    public static JSONNote createNote(JSONObject object){
        return new JSONNote(createFolder((JSONObject) object.get("note"), (String) object.get("name")));
    }

    private static Folder createFolder(JSONObject object, String title){
        ArrayList<Folder> folders = new ArrayList<>();
        ArrayList<Note> notes = new ArrayList<>();
        Folder folder = new Folder(folders, notes, title);
        for(Object key: object.keySet()){
            Object value = object.get((String) key);
            if(value instanceof JSONObject){
                folder.getFolders().add(createFolder((JSONObject) value, (String) key));
            } else {
                folder.getNotes().add(new Note((String) key, (String) value));
            }
        }
        return folder;
    }
}
