package ib.zpo2.L4;

import ib.zpo2.L2.AESKey;
import ib.zpo2.L2.CustomerMainKey;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;

public class NoteManager {

    public static String Directory= "src/main/java/ib/zpo2/L4/notesDirectory";

    public static String getPathNotes(String id){
        return Directory +"/"+id + ".txt";
    }


//    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//public static void saveImageAndMetadata(String image, String metadata, String customerId){
//    String id = UUID.randomUUID().toString();
//    Path imagePath = Paths.get("src/main/java/ib/zpo2/L2/imageDirectory/" + customerId + "/" + id + ".txt");
//    Path metadataPath = Paths.get("src/main/java/ib/zpo2/L2/imageDirectory/" + customerId + "/" + id + ".metadata.txt");
//
//    try {
//
//        Files.writeString(imagePath, image, StandardCharsets.UTF_8);
//        Files.writeString(metadataPath, metadata, StandardCharsets.UTF_8);
//    } catch (IOException ex) {
//        System.out.print("Invalid Path");
//    }
//}
//    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void saveNote(JSONNote jSONNote, String id, String key) throws IOException {
        String encrypted = AESKey.encrypt(jSONNote.toString(), key);
        File file = new File(getPathNotes(id));
        FileWriter writer= new FileWriter(file);
        writer.write(encrypted);
        writer.close();
    }

    public static JSONNote loadNote (String id, String key) throws IOException, ParseException {
        File file = new File(getPathNotes(id));
        FileReader reader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(reader);
        String encrypted = bufferedReader.readLine();
        bufferedReader.close();
        String decrypted = AESKey.decrypt(encrypted, key);
        JSONParser parser = new JSONParser();
        org.json.JSONObject object = (org.json.JSONObject) parser.parse(decrypted);
        return NoteCreator.createNote(object);
    }

//    public Footnote searchFor(long id, String key, String phrase) throws IOException, ParseException {
//        Note note = loadNote(id, key);
//        return searchSub(note, phrase);
//    }

//    public static void main(String[] args) throws IOException, ParseException, NoSuchAlgorithmException, InvalidKeySpecException {
//        String path = getPathNotes("1975/CPS/Wykład/Review");
//        System.out.println(path);
////        // not in use anymore
////        String note = loadNote("1975/CPS/Wykład/Review");
////        System.out.println(note);
//        String mainKey = "e40f57da-115a-4875-9055-d774f6253a43";
//        String decryptedMainKey = CustomerMainKey.decryptMainKey("1975", "nolitimerequiaegosumtecum");
//    }
}
