package ib.zpo2.L1;

import java.util.List;

public interface Validator {
    Boolean isValid(List<String> id, String token);
}