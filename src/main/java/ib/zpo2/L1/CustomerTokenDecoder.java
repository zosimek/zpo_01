package ib.zpo2.L1;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import ib.zpo2.L1.customer.CountryCode;
import ib.zpo2.L1.customer.Id;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static ib.zpo2.L1.Token.tokenDecode;

// Create a helper class CustomerTokenDecoder - the decode(string token) method
// should create and return a Customer object based on the token. Use known
// regular expressions and operations on String.
public class CustomerTokenDecoder {

    static final Pattern patternCT = Pattern.compile("\"([A-Z]{2})(\\d+)ex_([a-zA-Z0-9]{8}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{12})([\\d\\.]*\\d+)\"");


    public static Customer decode(String tokenToDecode) {

//      Decoding token — tokenToDecode.
        Token token = tokenDecode(tokenToDecode);

//      Parsing String to Json in order to filter the customerToken value
//      It is checked whether the JWT contains the customerToken field in its content.
//      If not, the appropriate exception is thrown.
        JsonObject jsonObject = (new JsonParser()).parse(token.getPayload()).getAsJsonObject();
        if (!jsonObject.has("customerToken")){
            throw new IllegalStateException("The customerToken has not been found!");
        }

        String customerToken = String.valueOf(jsonObject.get("customerToken"));

//      Breakdown of customerToken into usable values
//      It is checked if the customerToken type has a valid format.
//      If not, an exception is thrown.
        Matcher matcherCT = patternCT.matcher(customerToken);

        if (!matcherCT.matches()){
            throw new IllegalStateException("The customerToken is in the wrong format!");
        }

        CountryCode countryCode = new CountryCode(matcherCT.group(1));
        Id id = new Id(matcherCT.group(2));
        UUID externalId = UUID.fromString(matcherCT.group(3));
        List enabledServices = new ArrayList<Integer>();
        enabledServices.addAll(Arrays.asList((matcherCT.group(4)).split("\\.")));

//      Create a class representing the Customer object - it should contain all the fields
//      that can be separated from the customer token (use appropriate types)
        Customer customer = new Customer(countryCode, id, externalId, enabledServices);

        return customer;

    }
}
