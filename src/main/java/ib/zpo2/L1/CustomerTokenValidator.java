package ib.zpo2.L1;

import java.util.List;

import static ib.zpo2.L1.CustomerTokenDecoder.decode;

public class CustomerTokenValidator implements Validator {
    @Override
    public Boolean isValid(List<String> id, String token) {
        Customer customer = decode(token);
        Boolean valid = true;

//      This will check if you have access to the service you are trying to use
//      (Your service has identifier = 12). If not, the appropriate exception is returned
        for (int i = 0; i < id.size(); i++) {
            if (!customer.getEnableServices().contains(id.get(i))) {
                valid = false;
                break;
//                throw new IllegalStateException("The user is not authorised to use the service");
            }
        }
        return valid;
    }
}
