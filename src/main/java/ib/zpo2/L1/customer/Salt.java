package ib.zpo2.L1.customer;

import java.security.SecureRandom;

public class Salt {
    String salt;

    // from https://stackoverflow.com/questions/18142745/how-do-i-generate-a-salt-in-java-for-salted-hash#:~:text=%20How%20do%20I%20generate%20a%20SALT%20in,they%20match%2C%20the%20password%20is%20correct.%20More%20
    public static byte[] generateSalt() {
        SecureRandom random = new SecureRandom();
        byte[] bytes = new byte[20];
        random.nextBytes(bytes);
        return bytes;
    }
}
