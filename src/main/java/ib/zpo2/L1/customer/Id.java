package ib.zpo2.L1.customer;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Id {
    private String id;

    public Id(String id) {
        Pattern pattern = Pattern.compile("\\d+");
        Matcher matcher = pattern.matcher(id);

        if (matcher.matches()){
            this.id = id;
        }
        else {
            throw new IllegalStateException("The customerToken is in the wrong format!");
        }
    }

    public String getId() {
        return id;
    }
}
