package ib.zpo2.L1.customer;

import javax.validation.ValidationException;
import javax.validation.constraints.Size;

public class CountryCode {
    private String countryCode;

    public CountryCode(String countryCode) {
        if (countryCode.length() == 2) {
            this.countryCode = countryCode;
        }
        else{
            throw new ValidationException("Country code is in the wrong format!");
        }
    }

    public String getCountryCode() {
        return countryCode;
    }
}
