package ib.zpo2.L1;

import java.util.Base64;

// The token is converted into an object form (Token) which will contain all
// information from the token and facilitate the use of this information.
public class Token {
    private String header;
    private String payload;

    public Token(String header, String payload) {
        this.header = header;
        this.payload = payload;
    }

    public String getHeader() {
        return header;
    }

    public String getPayload() {
        return payload;
    }

    //  The tokenDecode(string token) method returns object Token
    public static Token tokenDecode(String tokenToDecode){

//      Token decryption
//      copied from https://www.baeldung.com/java-jwt-token-decode
        String[] chunks = tokenToDecode.split("\\.");
        Base64.Decoder decoder = Base64.getUrlDecoder();
        String header = new String(decoder.decode(chunks[0]));
        String payload = new String(decoder.decode(chunks[1]));

//      The token is converted into an object form (Token) which will contain all
//      information from the token and facilitate the use of this information.
        Token token = new Token(header, payload);

        return token;
    }
}
