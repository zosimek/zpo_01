package ib.zpo2.L1;

import ib.zpo2.L1.customer.CountryCode;
import ib.zpo2.L1.customer.Id;
import ib.zpo2.L1.customer.RandomUUID;
import ib.zpo2.L1.customer.Salt;

import java.util.List;
import java.util.UUID;

// Create a class representing the Customer object - it should contain all the fields
// that can be separated from the customer token (use appropriate types)
public class Customer {
    private CountryCode countryCode;
    private Id id;

    private String password;

    private String key;

    private byte[] salt;
    private UUID externalId;
    private List enableServices;

    public Customer(CountryCode countryCode, Id id, UUID externalId, List enableServices) {
        this.countryCode = countryCode;
        this.id = id;
        this.externalId = externalId;
        this.enableServices = enableServices;
    }

    public Customer(CountryCode countryCode, Id id, String password, UUID externalId, List enableServices) {
        this.countryCode = countryCode;
        this.id = id;
        this.password = password;
        this.key = UUID.randomUUID().toString();
        this.salt  = Salt.generateSalt();
        this.externalId = externalId;
        this.enableServices = enableServices;
    }

    public CountryCode getCountryCode() {
        return countryCode;
    }

    public Id getId() {return id;}

    public UUID getExternalId() {
        return externalId;
    }

    public List getEnableServices() {
        return enableServices;
    }


    public String getKey() {
        return key;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public byte[] getSalt() {
        return salt;
    }


    public void setSalt() {
        this.salt = Salt.generateSalt();
    }
}
