package ib.zpo2.L3;

public class CustomerImagePath {
    String mainDirectory;
    String userDirectory;

    public CustomerImagePath(String userDirectory) {
        this.mainDirectory = "src/main/java/ib/zpo2/L2/imageDirectory";
        this.userDirectory = "/" + userDirectory;
    }

    public String getMainDirectory() {
        return mainDirectory;
    }

    public void setMainDirectory(String mainDirectory) {
        this.mainDirectory = mainDirectory;
    }

    public String getUserDirectory() {
        return userDirectory;
    }

    public void setUserDirectory(String userDirectory) {
        this.userDirectory = userDirectory;
    }
}
