package ib.zpo2.L3;

public class Image {

    private long id;
    private byte [] photo;
    private Metadata metadata;


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // CONSTRUCTOR , SETTERS and GETTERS

    public Image(byte[] photo, Metadata metadata) {
        this.photo = photo;
        this.metadata = metadata;
    }

    public Image(long id, byte[] photo, Metadata metadata) {
        this.id = id;
        this.photo = photo;
        this.metadata = metadata;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }
}
