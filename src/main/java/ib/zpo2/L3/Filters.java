package ib.zpo2.L3;

import java.time.LocalDate;
import java.util.ArrayList;

public class Filters {
    private String author;
    private String title;
    private String location;
    private LocalDate startDate;
    private LocalDate endDate;
    private String description;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // CONSTRUCTOR , SETTERS and GETTERS

    public Filters() {
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // FUNCTIONS

    private ArrayList<Image> filterTitle(ArrayList<Image> images){
        if(title == null) return images;
        ArrayList<Image> filteredImages = new ArrayList<>();
        for (int i = 0; i < images.size(); i++){
            if((images.get(i).getMetadata().getTitle().toLowerCase().contains(title.toLowerCase())))
                filteredImages.add(images.get(i));
        }
        return filteredImages;
    }

    private ArrayList<Image> filterDescription(ArrayList<Image> images){
        if (description == null) return images;
        ArrayList<Image> filteredImages = new ArrayList<>();
        for (int i = 0; i < images.size(); i++){
            if((images.get(i).getMetadata().getDescription().toLowerCase().contains(description.toLowerCase())))
                filteredImages.add(images.get(i));
        }
        return filteredImages;
    }

    private ArrayList<Image> filterLocation(ArrayList<Image> images){
        if (location == null) return images;
        ArrayList<Image> filteredImages = new ArrayList<>();
        for (int i = 0; i < images.size(); i++){
            if(location.equalsIgnoreCase(images.get(i).getMetadata().getLocation()))
                filteredImages.add(images.get(i));
        }
        return filteredImages;
    }

    private ArrayList<Image> filterAuthor(ArrayList<Image> images){
        if (author == null) return images;
        ArrayList<Image> filteredImages = new ArrayList<>();
        for (int i = 0; i < images.size(); i++){
            if(author.equalsIgnoreCase(images.get(i).getMetadata().getAuthor()))
                filteredImages.add(images.get(i));
        }
        return filteredImages;
    }

    private ArrayList<Image> filterDateStart(ArrayList<Image> images){
        if (startDate == null) return images;
        ArrayList<Image> filteredImages = new ArrayList<>();
        for (int i = 0; i < images.size(); i++){
            if(!(images.get(i).getMetadata().getDate().isBefore(startDate)))
                filteredImages.add(images.get(i));
        }
        return filteredImages;
    }

    private ArrayList<Image> filterDateEnd(ArrayList<Image> images){
        if (endDate == null) return images;
        ArrayList<Image> filteredImages = new ArrayList<>();
        for (int i = 0; i < images.size(); i++){
            if(!(images.get(i).getMetadata().getDate().isAfter(endDate)))
                filteredImages.add(images.get(i));
        }
        return filteredImages;
    }

        public ArrayList<Image> filter(ArrayList<Image> images){
        return filterDateEnd(filterDateStart(filterAuthor(filterLocation(filterDescription(filterTitle(images))))));
    }
}
