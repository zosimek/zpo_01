package ib.zpo2.L3;

import java.time.LocalDate;

public class Metadata {

    private String author;
    private String title;
    private String location;
    private LocalDate date;
    private String description;
    private String format;


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // CONSTRUCTOR , SETTERS and GETTERS

    public Metadata(String author, String title, String location, LocalDate date, String description, String format) {
        this.author = author;
        this.title = title;
        this.location = location;
        this.date = date;
        this.description = description;
        this.format = format;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }
}
