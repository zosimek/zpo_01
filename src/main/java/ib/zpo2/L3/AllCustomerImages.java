package ib.zpo2.L3;

import ib.zpo2.L2.ImageDecoding;
import ib.zpo2.L2.MetaDecoding;
import org.json.JSONObject;

import java.io.File;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDate;
import java.util.ArrayList;

public class AllCustomerImages {
    String mainDirectory;
    ArrayList<Image> images;
    ArrayList<String> imageFiles;
    ArrayList<String> metadataFiles;

    public AllCustomerImages() {
        this.mainDirectory = "src/main/java/ib/zpo2/L2/imageDirectory";
        this.images = new ArrayList<Image>();
        this.imageFiles = new ArrayList<String>();
        this.metadataFiles = new ArrayList<String>();

    }

    public ArrayList<Image> getImages() {
        return images;
    }


    public ArrayList<String> getImageFiels() {
        return imageFiles;
    }

    public ArrayList<String> getMetadataFiels() {
        return metadataFiles;
    }

    public static void findAllFilesInFolder(File folder, AllCustomerImages allCustomerImages) {
        // get all file names from the directory and store them in dedicated ArrayLists

        for (File file : folder.listFiles()) {
            if(file.getName().contains("metadata")){
                allCustomerImages.metadataFiles.add(file.getName());
            }else{
                allCustomerImages.imageFiles.add(file.getName());
            }
        }
    }

    public static void encodeAndStore(ArrayList<String> imageFiles, ArrayList<String> metadataFiles, String customerId, String customerPassword, AllCustomerImages allCustomerImages) throws NoSuchAlgorithmException, InvalidKeySpecException {
        for(int i = 0; i < imageFiles.size(); i++){
            String imageString = ImageDecoding.imageDecoder(customerId, customerPassword, "src/main/java/ib/zpo2/L2/imageDirectory/" + customerId + "/" + imageFiles.get(i));
            byte[] imageByte = ImageDecoding.imageBase64Decoder(imageString);

            JSONObject metadataDecrypted = MetaDecoding.metadataDecoder(customerId, customerPassword, "src/main/java/ib/zpo2/L2/imageDirectory/" + customerId + "/" + metadataFiles.get(i));
            Metadata metadata = new Metadata(metadataDecrypted.getString("author"), metadataDecrypted.getString("title"), metadataDecrypted.getString("location"), LocalDate.parse(metadataDecrypted.getString("date")), metadataDecrypted.getString("description"), metadataDecrypted.getString("format"));

            Image image = new Image(imageByte, metadata);
            allCustomerImages.images.add(image);

        }
    }



//    // Commented out 'cause main function created wa just to check if all above work as expected
//    public static void main(String[] args) throws NoSuchAlgorithmException, InvalidKeySpecException {
//        AllCustomerImages allCustomerImages = new AllCustomerImages();
//        String wholePath = "src/main/java/ib/zpo2/L2/imageDirectory/1975";
//        File folder = new File(wholePath);
//        findAllFilesInFolder(folder, allCustomerImages);
//        encodeAndStore(allCustomerImages.getImageFiels(),allCustomerImages.getMetadataFiels(), "1975", "nolitimerequiaegosumtecum", allCustomerImages);
//        System.out.println(allCustomerImages.getImageFiels());
//        System.out.println(allCustomerImages.getMetadataFiels());
//        System.out.println(allCustomerImages.getImages().get(0).getMetadata().getDescription());
//    }
}
